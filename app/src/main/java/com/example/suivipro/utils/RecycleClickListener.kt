package com.example.suivipro.utils

import android.view.View
import com.example.suivipro.model.Menu
import com.example.suivipro.model.Projet

interface RecycleClickListener {
    fun onClick(menu: Menu) {}
    fun onClick(projet: Projet) {}
    fun onViewClick(view: View, menu: Menu) {}
}