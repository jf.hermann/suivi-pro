package com.example.suivipro.model

data class Menu(
    val name: String? = null,
    val image: Int? = null
)