package com.example.suivipro.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Projet(
    val name: String? = null,
    val image: Int? = null,
    val referent: String? = null,
    val delaisEmis: Int? = null,
    val delaisRestant: Int? = null,
    val state: Boolean? = true
): Parcelable