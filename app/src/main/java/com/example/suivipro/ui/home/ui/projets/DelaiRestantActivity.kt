package com.example.suivipro.ui.home.ui.projets

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.suivipro.R
import kotlinx.android.synthetic.main.activity_delai_restant.*

class DelaiRestantActivity : AppCompatActivity() {
    private var delai: Int? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delai_restant)
        supportActionBar?.hide()

        delai = intent.getIntExtra("delais",0)
        tvDelai.text ="$delai JOURS"

        ivClose.setOnClickListener {
            finish()
        }

        ivBack.setOnClickListener {
            finish()
        }
    }
}