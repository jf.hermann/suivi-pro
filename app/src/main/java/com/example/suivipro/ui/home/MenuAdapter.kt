package com.example.suivipro.ui.home

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.suivipro.R
import com.example.suivipro.model.Menu
import com.example.suivipro.utils.RecycleClickListener
import kotlinx.android.synthetic.main.item_drawer_menu.view.*


class MenuAdapter(context: Context, menus: ArrayList<Menu>) :
    RecyclerView.Adapter<MenuAdapter.SingleViewHolder?>() {
    private val context: Context = context
    lateinit var listener: RecycleClickListener
    private var menu: ArrayList<Menu>

    // if checkedPosition = -1, there is no default selection
    // if checkedPosition = 0, 1st item is selected by default
    private var checkedPosition = 0
    fun setMenu(employees: ArrayList<Menu>) {
        this.menu = ArrayList()
        this.menu = employees
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): SingleViewHolder {
        val view: View =
            LayoutInflater.from(context).inflate(R.layout.item_drawer_menu, viewGroup, false)
        return SingleViewHolder(view)
    }

    override fun onBindViewHolder(
        singleViewHolder: SingleViewHolder,
        position: Int
    ) {
        singleViewHolder.bind(menu[position])
    }

    override fun getItemCount(): Int {
        return menu.size
    }

    inner class SingleViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        private val textView: TextView = itemView.tvMenu
        private val imageView: ImageView = itemView.ivMenu
        private val root: LinearLayout = itemView.llRoot

        fun bind(menu: Menu) {
            if (checkedPosition == -1) {
                //imageView.setVisibility(View.GONE)
                textView.setTextColor(Color.parseColor("#FFFFFF"))
                imageView.setColorFilter(Color.parseColor("#FFFFFF"))
                root.background.clearColorFilter()
            } else {
                if (checkedPosition == adapterPosition) {
                    imageView.visibility = View.VISIBLE
                    textView.setTextColor(Color.parseColor("#000000"))
                    imageView.setColorFilter(Color.parseColor("#000000"))
                    root.background.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP)
                } else {
                    //imageView.setVisibility(View.GONE)
                    textView.setTextColor(Color.parseColor("#FFFFFF"))
                    imageView.setColorFilter(Color.parseColor("#FFFFFF"))
                    root.background.clearColorFilter()
                }
            }
            textView.text = menu.name
            imageView.load(menu .image!!)

            itemView.setOnClickListener {
                imageView.visibility = View.VISIBLE
                textView.setTextColor(Color.parseColor("#000000"))
                imageView.setColorFilter(Color.parseColor("#000000"))
                root.background.setColorFilter(Color.parseColor("#FFFFFF"), PorterDuff.Mode.SRC_ATOP)
                listener.onViewClick(itemView, menu)
                if (checkedPosition != getAdapterPosition()) {
                    notifyItemChanged(checkedPosition)
                    checkedPosition = adapterPosition
                }
            }
        }

    }

    fun getSelected(): Menu? {
        return if (checkedPosition != -1) {
            menu[checkedPosition]
        } else null
    }

    init {
        this.menu = menus
    }
}