package com.example.suivipro.ui.home.ui.projets

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.suivipro.R
import com.example.suivipro.model.Projet
import com.example.suivipro.ui.home.HomeActivity
import com.example.suivipro.utils.RecycleClickListener
import kotlinx.android.synthetic.main.fragment_projets.*

class ProjetsFragment : Fragment(), RecycleClickListener {

    private val adapterDown = ProjetAdapter()

    private var listDown = mutableListOf<Projet>()

    private lateinit var mActivity: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_projets, container, false)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mActivity = activity as HomeActivity
        adapterDown.listener = this
        rvDown.adapter = adapterDown

        listDown.add(
            Projet(
                image = R.drawable.people_sitting_beside_brown_wooden_desk_near_flat_screen_tv1181355,
                name = "Maquette Smile",
                delaisEmis = 15,
                delaisRestant = 30,
                state = false
            )
        )
        listDown.add(
            Projet(
                image = R.drawable.pexels_fauxels_3183150,
                name = "Maquette Orange",
                delaisRestant = 5,
                delaisEmis = 15,
                state = false
            )
        )
        listDown.add(
            Projet(
                image = R.drawable.three_woman_and_man_wearing_apron_763934,
                name = "Maquette Objectif Libre",
                delaisEmis = 20,
                delaisRestant = 10
            )
        )
        listDown.add(
            Projet(
                image = R.drawable.top_view_photo_of_people_discussing_3182774,
                name = "Maquette SmartPay",
                delaisEmis = 2,
                delaisRestant = 3,
                state = false
            )
        )
        adapterDown.submitList(listDown)

        ivMenu.setOnClickListener {
            mActivity.showMenu()
        }

        llProfil.setOnClickListener {
            mActivity.onNavigationDrawerItemSelected(1)
        }

        llDiscussion.setOnClickListener {
            mActivity.onNavigationDrawerItemSelected(2)
        }

    }

    override fun onClick(projet: Projet) {
        val intent = Intent(activity, ProjetActivity::class.java)
        intent.putExtra("projet", projet)
        startActivity(intent)
    }
}