package com.example.suivipro.ui.home
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.navigation.ui.AppBarConfiguration
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.example.suivipro.R
import com.example.suivipro.model.Menu
import com.example.suivipro.ui.home.ui.discution.DiscussionFragment
import com.example.suivipro.ui.home.ui.profil.ContactFragment
import com.example.suivipro.ui.home.ui.projets.ProjetsFragment
import com.example.suivipro.ui.login.LoginActivity
import com.example.suivipro.ui.settings.SettingsActivity
import com.example.suivipro.utils.RecycleClickListener
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), RecycleClickListener {
    private lateinit var menuAdapter: MenuAdapter
    private var menu = ArrayList<Menu>()

    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        drawer_layout.openDrawer(Gravity.LEFT)
        drawer_layout.closeDrawer(Gravity.LEFT)



        menu.add(Menu(name = "Mes projets", image = R.drawable.productivity_01))
        menu.add(Menu(name = "Discussion", image = R.drawable.smartphone_01))
        menu.add(Menu(name = "Profil", image = R.drawable.profil))

        menuAdapter = MenuAdapter(this, menu)
        menuAdapter.listener = this

        rvMenu.adapter = menuAdapter

        menuAdapter.setMenu(menu)

        init()

        llLogout.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finishAffinity()
        }

        llSettings.setOnClickListener {
            startActivity(Intent(this, SettingsActivity::class.java))
        }
    }

    fun showMenu() {
        drawer_layout.openDrawer(Gravity.LEFT)
    }

    private fun init() {
        // update the main content by replacing fragments
        val fragmentManager: androidx.fragment.app.FragmentManager = supportFragmentManager
        fragmentManager.beginTransaction()
            .replace(R.id.detail_fragment_container, ProjetsFragment())
            .commit()
    }


    fun onNavigationDrawerItemSelected(position: Int) {
        // update the main content by replacing fragments
        val fragmentManager: androidx.fragment.app.FragmentManager =
            supportFragmentManager // For AppCompat use getSupportFragmentManager
        val fragment: androidx.fragment.app.Fragment = when (position) {
            0 -> ProjetsFragment()
            1 -> ContactFragment()
            2 -> DiscussionFragment()
            /*3 -> DiscutionFragment()*/
            else -> ProjetsFragment()
        }
        fragmentManager.beginTransaction()
            .replace(R.id.detail_fragment_container, fragment)
            .commit()
    }

    override fun onViewClick(view: View, menu: Menu) {
        when (menu.name) {
            "Mes projets" -> {
                onNavigationDrawerItemSelected(0)
                drawer_layout.closeDrawer(Gravity.LEFT)
            }
            "Discussion" -> {
                onNavigationDrawerItemSelected(2)
                drawer_layout.closeDrawer(Gravity.LEFT)
            }
            "Profil" -> {
                onNavigationDrawerItemSelected(1)
                drawer_layout.closeDrawer(Gravity.LEFT)
            }
        }
    }
}