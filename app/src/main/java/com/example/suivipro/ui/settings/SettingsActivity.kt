package com.example.suivipro.ui.settings

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.suivipro.R
import kotlinx.android.synthetic.main.activity_settings.*

class SettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        supportActionBar?.hide()

        ivBack.setOnClickListener {
            finish()
        }
    }
}