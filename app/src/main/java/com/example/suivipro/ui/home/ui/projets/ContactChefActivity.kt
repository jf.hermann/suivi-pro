package com.example.suivipro.ui.home.ui.projets

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.suivipro.R
import kotlinx.android.synthetic.main.activity_contact_chef.*

class ContactChefActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_chef)
        supportActionBar?.hide()

        ivBack.setOnClickListener {
            finish()
        }
    }
}