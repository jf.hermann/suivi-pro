package com.example.suivipro.ui.home.ui.projets

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.suivipro.R
import com.example.suivipro.model.Projet
import com.example.suivipro.utils.RecycleClickListener
import kotlinx.android.synthetic.main.item_home.view.*

class ProjetAdapter :
    ListAdapter<Projet, ProjetAdapter.VideosHolder>(
        VideosDiffCallback()
    ) {


    lateinit var listener: RecycleClickListener

    class VideosDiffCallback : DiffUtil.ItemCallback<Projet>() {

        override fun areItemsTheSame(oldItem: Projet, newItem: Projet): Boolean {
            return oldItem.image == newItem.image
        }

        override fun areContentsTheSame(oldItem: Projet, newItem: Projet): Boolean {
            return oldItem == newItem
        }

    }

    class VideosHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.ivImg
        val name = itemView.tvNameProjet
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideosHolder {
        return VideosHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_home, parent, false)
        )
    }

    override fun onBindViewHolder(holder: VideosHolder, position: Int) {

        val item = getItem(position)

        holder.image.load(item.image!!)

        holder.name.text = item.name!!

        holder.itemView.setOnClickListener{
            listener.onClick(item)
        }
    }
}