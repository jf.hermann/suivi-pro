package com.example.suivipro.ui.home.ui.profil

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.suivipro.R
import com.example.suivipro.ui.home.HomeActivity
import kotlinx.android.synthetic.main.fragment_contact.*

class ContactFragment : Fragment() {

    private lateinit var mActivity: HomeActivity

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_contact, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mActivity = activity as HomeActivity

        ivMenu.setOnClickListener {
            mActivity.showMenu()
        }

        llDiscussion.setOnClickListener {
            mActivity.onNavigationDrawerItemSelected(2)
        }

        llProjets.setOnClickListener {
            mActivity.onNavigationDrawerItemSelected(0)
        }
    }
}