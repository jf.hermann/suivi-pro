package com.example.suivipro.ui.home.ui.projets

import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import coil.load
import com.example.suivipro.R
import com.example.suivipro.model.Projet
import kotlinx.android.synthetic.main.activity_projet.*

class ProjetActivity : AppCompatActivity() {
    private var projet: Projet? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_projet)
        supportActionBar?.hide()

        projet = intent.getParcelableExtra("projet")

        iv.load(projet!!.image!!)
        tvNameProjet.text = projet!!.name

        when (projet!!.state) {
            true -> {
                tvState.text = "Projet en cours"
            }
            false -> {
                tvState.text = "Projet terminé"
                llState.background.setColorFilter(
                    Color.parseColor("#ffa500"),
                    PorterDuff.Mode.SRC_ATOP
                )
            }
        }

        ivBack.setOnClickListener {
            finish()
        }

        llReferent.setOnClickListener {
            startActivity(Intent(this, ReferentActivity::class.java))
        }

        llDelaiEmis.setOnClickListener {
            val intent = Intent(this, DelaiEmisActivity::class.java)
            intent.putExtra("delais", projet!!.delaisEmis)
            startActivity(intent)
        }

        llContact.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+225 87 24 49 54")
            startActivity(intent)
        }

        llDelaiRestant.setOnClickListener {
            val intent = Intent(this, DelaiRestantActivity::class.java)
            intent.putExtra("delais", projet!!.delaisRestant)
            startActivity(intent)
        }

        tvView.setOnClickListener {
            val uri: Uri =
                Uri.parse("http://www.google.com") // missing 'http://' will cause crashed
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }
}