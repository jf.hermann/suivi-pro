package com.example.suivipro.ui.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.suivipro.R
import com.example.suivipro.ui.home.HomeActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        btnSubmit.setOnClickListener {
            if (checkform()) login()
        }
    }

    private fun checkform() : Boolean {
        if (editEmail.text.isEmpty()){
            Toast.makeText(this, "l'adresse e-mail est vide", Toast.LENGTH_SHORT).show()
            editEmail.requestFocus()
            return false
        }

        if (editPassword.text.isEmpty()){
            Toast.makeText(this, "le mot de passe est vide", Toast.LENGTH_SHORT).show()
            editPassword.requestFocus()
            return false
        }
        return true
    }

    private fun login() {
        if ((editEmail.text.toString() == "franck.kouassi@smile.ci") && (editPassword.text.toString() == "kouassipassword")) {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            Toast.makeText(this, "les accès sont incorrects", Toast.LENGTH_SHORT).show()
        }
    }
}