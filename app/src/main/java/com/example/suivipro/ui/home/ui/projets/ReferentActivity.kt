package com.example.suivipro.ui.home.ui.projets

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.suivipro.R
import kotlinx.android.synthetic.main.activity_referent.*

class ReferentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_referent)
        supportActionBar?.hide()

        ivClose.setOnClickListener {
            finish()
        }

        ivBack.setOnClickListener {
            finish()
        }
    }
}